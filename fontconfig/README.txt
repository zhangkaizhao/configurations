# fontconfig

Default fonts:

* sans-serif: Lato
* serif: Merriweather
* monospace: Ubuntu Mono

Fallback for CJK fonts: Source Han fonts, WenQuanYi fonts, Arphic fonts.

----

Q: Why Source Han fonts over other CJK fonts for CJK fonts?

A: Because Source Han fonts have Bold font style and the others only have regular font style.
   And the others which do not base on Source Han fonts have not enought CJK characters.
